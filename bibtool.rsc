preserve.key.case        = on
preserve.keys            = on

new.entry.type           = "software"

print.use.tab            = off

rewrite.rule             = { month # "jan" # "1" }
rewrite.rule             = { month # "feb" # "2" }
rewrite.rule             = { month # "mar" # "3" }
rewrite.rule             = { month # "apr" # "4" }
rewrite.rule             = { month # "may" # "5" }
rewrite.rule             = { month # "jun" # "6" }
rewrite.rule             = { month # "jul" # "7" }
rewrite.rule             = { month # "aug" # "8" }
rewrite.rule             = { month # "sep" # "9" }
rewrite.rule             = { month # "oct" # "10" }
rewrite.rule             = { month # "nov" # "11" }
rewrite.rule             = { month # "dec" # "12" }

rewrite.rule             = { pages # "\([0-9]+\) ?- ?\([0-9]+\)" # "\1--\2" }

rewrite.rule             = {"^\"\([^#]*\)\"$" # "{\1}"}

sort                     = on

sort.order               = { * =
  author
  # title
  # publisher
  # booktitle
  # edition
  # journal
  # series
  # volume
  # number
  # articleno
  # pages
  # numpages
  # year
  # month
  # day
  # issn
  # doi
  # reportnumber
  # keywords
  # eprint
  # archiveprefix
  # primaryclass
  # url
  # pdf
  # abstract
  # note
}

check.double             = on
check.double.delete      = on
unique.field             = { eprint }
unique.field             = { doi }

count.used               = on

print.line.length        = 100
