#!/usr/bin/env bash

tar -cvf submission_arxiv_v2.tar.gz paper.tex paper.bbl jheppub.sty \
  figures/*.pdf \
  img/training-box.pdf \
  img/abs_diff_part_box.pdf \
  img/abs_diff_eps_box.pdf \
  img/rel_diff_eps_box.pdf \
  img/ratio_eps_box.pdf \
  img/ps_box.pdf \
  img/region_t331ZZZM.pdf \
  img/training-t331ZZZM.pdf \
  img/rel_diff_eps_t331ZZZM.pdf \
  img/ratio_eps_t331ZZZM.pdf \
  img/region_heavycrossbox.pdf \
  img/training-heavycrossbox.pdf \
  img/rel_diff_eps_heavycrossbox.pdf \
  img/ratio_eps_heavycrossbox.pdf \
  img/region_topbox.pdf \
  img/training-topbox.pdf \
  img/rel_diff_eps_topbox.pdf \
  img/ratio_eps_topbox.pdf \
  img/abs_diff_part_t331ZZZM.pdf \
  img/abs_diff_eps_t331ZZZM.pdf \
  img/abs_diff_ps_histo_t331ZZZM.pdf \
  img/abs_diff_part_heavycrossbox.pdf \
  img/abs_diff_eps_heavycrossbox.pdf \
  img/abs_diff_ps_scatter_heavycrossbox.pdf \
  img/abs_diff_part_topbox.pdf \
  img/abs_diff_eps_topbox.pdf \
  img/abs_diff_ps_scatter_topbox.pdf

