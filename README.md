# [paper-feynman-integrals-nn](https://gitlab.com/feynman-integrals-nn/paper-feynman-integrals-nn)

Build with 

```shell
pipenv install -e <path to feynman-integrals-nn repo>
pipenv install
pipenv run ./slim-stats.py l <path to integral family>  # only if you don't want to use those committed to the repo
pipenv run ./plots.py -f -s <path to models> <path to datasets>
latexmk -pdf
```

or similar.
