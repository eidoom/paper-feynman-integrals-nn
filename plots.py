#!/usr/bin/env python3

import pathlib, argparse, math, time
import numpy, awkward, matplotlib.pyplot, matplotlib.cm, torch, matplotlib.patheffects
from finn.lib.inference import Ensemble, load_model
from finn.lib.configuration import get_cfg, ps_params
from finn.lib.derivative_difference import DerivativeDifference

# PHI = (1 + math.sqrt(5)) / 2
R = 3 / 4
W = 4.8

LABELS_PARTS = ("Real", "Imaginary")


def get_args():
    parser = argparse.ArgumentParser(description="Generate paper plots")

    parser.add_argument(
        "model",
        type=pathlib.Path,
        help="Path to models directory.",
    )
    parser.add_argument(
        "data",
        type=pathlib.Path,
        nargs="?",
        help="Path to datasets directory (only include if different to model).",
    )
    parser.add_argument(
        "-f",
        "--final",
        action="store_true",
        help="Use TeX font.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase verbosity.",
    )
    parser.add_argument(
        "-d",
        "--device",
        type=torch.device,
        default="cpu",
        help="Set PyTorch device [default: cpu].",
    )
    parser.add_argument(
        "-p",
        "--suppress-plots",
        action="store_true",
        help="Don't make plots.",
    )
    parser.add_argument(
        "-s",
        "--suppress-statistics",
        action="store_true",
        help="Don't show statistics.",
    )
    parser.add_argument(
        "-c",
        "--cache",
        action="store_true",
        help="Create and use existing caches of intermediate results.",
    )

    args = parser.parse_args()

    if args.data is None:
        args.data = args.model

    return args


def get_training_data(fam, reps_re, reps_im, n=10, v=True):
    """
    output shape: (
        re/im (part),
        replica,
        (
            t (epoch_end_times),
            lr (learning_rate_values),
            avg (training metric mean),
            min (training metric min),
            max (training metric max),
        ),
        epoch,
    )
    """
    folder = fam / f"{n}-replicas"
    if v:
        print(f"Reading from {folder}")
    return awkward.Array(
        {
            part: [
                {
                    k: v
                    for k, v in zip(
                        ("t", "lr", "avg", "min", "max"),
                        numpy.load(folder / part / rep / "training-statistics.npy"),
                    )
                }
                for rep in reps
            ]
            for part, reps in zip(("re", "im"), (reps_re, reps_im))
        }
    )


def plot_training_statistics(img, name, metrics, v=True, triple=True):
    """
    learning curve
    """
    file = img / f"training-{name}.pdf"
    if v:
        print(f"Plotting {file}")

    part = metrics["re"]

    fms = part[:, "avg", -1]
    min_fm = numpy.argmin(fms)
    mean_fm = numpy.mean(fms)
    if triple:
        select = numpy.array(
            [numpy.argmax(fms), numpy.argmin(abs(fms - mean_fm)), min_fm]
        )
    else:
        select = numpy.array((min_fm,))

    labels = ("Worst", "Middle", "Best") if triple else ("Best",)
    colours = matplotlib.cm.tab10(range(len(labels)))

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    ax.set_xlabel("Epoch")
    ax.set_ylabel("Loss")

    ax.set_yscale("log")

    for rep, label, colour in zip(part[select], labels, colours):
        epoch = numpy.arange(1, len(rep["t"]) + 1)
        ax.fill_between(
            epoch,
            rep["min"],
            rep["max"],
            alpha=0.2,
            facecolor=colour,
            edgecolor=None,
        )
        ax.plot(epoch, rep["avg"], label=label, color=colour, linestyle="solid")

    ax.set_ylim(top=3 * max(part[select, "avg", 0]))

    if triple:
        ax.legend(loc="upper right", reverse=True)

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def get_testing_data(root, cfg, v=True):
    fin = root / "testing_input.csv"

    if v:
        print(f"Loading {fin}")

    input = numpy.loadtxt(fin, delimiter=",", ndmin=2)

    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    fouts = [root / part / "testing_output.csv" for part in ("re", "im")]

    if v:
        for fout in fouts:
            print(f"Loading {fout}")

    output = numpy.stack(
        [
            numpy.loadtxt(fout, delimiter=",", ndmin=2).reshape(
                len(input), num_fn, num_eps
            )
            for fout in fouts
        ],
        axis=3,
    )

    return input, output


def isolate_part(y):
    num_in, num_fn, num_eps, num_prt = y.shape
    return y.reshape(num_in * num_fn * num_eps, num_prt)


def isolate_eps(y):
    num_in, num_fn, num_eps, num_prt = y.shape
    return y.swapaxes(2, 3).reshape(num_in * num_fn * num_prt, num_eps)


def isolate_fn(y):
    num_in, num_fn, num_eps, num_prt = y.shape
    return numpy.moveaxis(y, 1, 3).reshape(num_in * num_eps * num_prt, num_fn)


def labels_eps(y):
    num_eps = y.shape[2]
    return [str(i) for i in range(num_eps)]


def labels_fn(y):
    num_fn = y.shape[1]
    return [rf"$h_{{{i}}}$" for i in range(1, num_fn + 1)]


def multiplier(p):
    return rf" ($\times10^{{-{p}}}$)" if p else ""


def plot_abs_diff(
    img,
    kind,
    name,
    ad,
    bins=None,
    labels=None,
    v=True,
    log=False,
    q=0.001,
    ltit=None,
):
    """
    absolute difference
    """
    file = img / f"abs_diff_{kind}_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    fad = ad.compressed()

    xrange = numpy.quantile(fad, (q, 1))

    nzad = fad[fad != 0]

    b = 10 ** numpy.histogram_bin_edges(
        numpy.log10(nzad),
        range=numpy.log10(xrange),
        bins="auto" if bins is None else bins,
    )

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    ax.set_xscale("log")

    if log:
        ax.set_yscale("log")

    ax.set_ylabel(r"Proportion (\%)")
    ax.set_xlabel("Absolute difference")

    if ad.ndim == 1:
        ax.hist(
            fad,
            bins=b,
            histtype="step",
            weights=numpy.full_like(fad, 100 / len(fad)),
        )
    else:
        for part, label in zip(ad.T, labels):
            part = part.compressed()

            if len(part):
                ax.hist(
                    part,
                    bins=b,
                    histtype="step",
                    weights=numpy.full_like(part, 100 / len(part)),
                    label=label,
                )

        if labels is not None:
            ax.legend(title=ltit)

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_abs_diff_cum(img, ad, name, bins, v):
    """
    absolute difference (cumulative)
    """
    plot_abs_diff(
        img,
        "cum",
        name,
        numpy.ravel(ad),
        bins=bins,
        v=v,
    )


def plot_abs_diff_part(img, ad, name, bins, v):
    """
    absolute difference (real vs imaginary)
    """
    plot_abs_diff(
        img,
        "part",
        name,
        isolate_part(ad),
        bins=bins,
        labels=LABELS_PARTS,
        v=v,
    )


def plot_abs_diff_eps(img, ad, name, bins, v):
    """
    absolute difference (eps orders)
    """
    plot_abs_diff(
        img,
        "eps",
        name,
        isolate_eps(ad),
        bins=bins,
        labels=labels_eps(ad),
        v=v,
        ltit=r"$w$",
    )


def plot_abs_diff_fn(img, ad, name, bins, v):
    """
    absolute difference (integral functions)
    unused
    """
    plot_abs_diff(
        img,
        "fn",
        name,
        isolate_fn(ad),
        bins=bins,
        labels=labels_fn(ad),
        v=v,
    )


def plot_ratio(
    img,
    kind,
    name,
    rat,
    bins=None,
    log=False,
    q=0.01,
    labels=None,
    v=True,
    nticks=6,
    p=0,
    ltit=None,
):
    """
    logarithm of ratio
    """
    file = img / f"ratio_{kind}_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    rat = rat * 10**p

    frat = rat.compressed()

    xrange = numpy.quantile(frat, (q, 1 - q))

    b = numpy.histogram_bin_edges(
        frat,
        range=xrange,
        bins="auto" if bins is None else bins,
    )

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    if log:
        ax.set_yscale("log")

    ax.set_xlim(xrange)

    ax.set_ylabel(r"Proportion (\%)")
    ax.set_xlabel(f"Logarithm of ratio{multiplier(p)}")

    if rat.ndim == 1:
        ax.hist(
            frat,
            bins=b,
            histtype="step",
            weights=numpy.full_like(frat, 100 / len(frat)),
        )
    else:
        for part, label in zip(rat.T, labels):
            part = part.compressed()

            if len(part):
                ax.hist(
                    part,
                    bins=b,
                    histtype="step",
                    weights=numpy.full_like(part, 100 / len(part)),
                    label=label,
                )

        if labels is not None:
            ax.legend(title=ltit)

    ax.xaxis.set_major_locator(matplotlib.pyplot.MaxNLocator(nticks))

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_ratio_cum(img, rat, name, bins, v, p):
    """
    logarithm of ratio (cumulative)
    """
    plot_ratio(
        img,
        "cum",
        name,
        numpy.ravel(rat),
        bins=bins,
        v=v,
        p=p,
    )


def plot_ratio_part(img, rat, name, bins, v, p):
    """
    logarithm of ratio (real vs imaginary)
    """
    plot_ratio(
        img,
        "part",
        name,
        isolate_part(rat),
        bins=bins,
        labels=LABELS_PARTS,
        v=v,
        p=p,
    )


def plot_ratio_eps(img, rat, name, bins, v, p):
    """
    logarithm of ratio (eps orders)
    """
    plot_ratio(
        img,
        "eps",
        name,
        isolate_eps(rat),
        bins=bins,
        labels=labels_eps(rat),
        v=v,
        p=p,
        ltit=r"$w$",
    )


def plot_ratio_fn(img, rat, name, bins, v, p):
    """
    logarithm of ratio (integral functions)
    unused
    """
    plot_ratio(
        img,
        "fn",
        name,
        isolate_fn(rat),
        bins=bins,
        labels=labels_fn(rat),
        v=v,
        p=p,
    )


def plot_rel_diff(
    img,
    kind,
    name,
    rd,
    bins=None,
    labels=None,
    v=True,
    log=False,
    q=0.001,
    ltit=None,
):
    """
    magnitude of relative difference
    """
    file = img / f"rel_diff_{kind}_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    frd = rd.compressed()

    xrange = numpy.quantile(frd, (q, 1))

    nzrd = frd[frd != 0]

    b = 10 ** numpy.histogram_bin_edges(
        numpy.log10(nzrd),
        range=numpy.log10(xrange),
        bins="auto" if bins is None else bins,
    )

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    ax.set_xscale("log")

    if log:
        ax.set_yscale("log")

    ax.set_ylabel(r"Proportion (\%)")
    ax.set_xlabel("Magnitude of relative difference")

    if rd.ndim == 1:
        ax.hist(
            rd,
            bins=b,
            histtype="step",
            weights=numpy.full_like(rd, 100 / len(rd)),
        )
    else:
        for part, label in zip(rd.T, labels):
            part = part.compressed()

            if len(part):
                ax.hist(
                    part,
                    bins=b,
                    histtype="step",
                    weights=numpy.full_like(part, 100 / len(part)),
                    label=label,
                )

        if labels is not None:
            ax.legend(title=ltit)

    fig.savefig(file, bbox_inches="tight")


def plot_rel_diff_cum(img, rd, name, bins, v):
    """
    magnitude of relative difference (cumulative)
    """
    plot_rel_diff(
        img,
        "cum",
        name,
        numpy.ravel(rd),
        bins=bins,
        v=v,
    )


def plot_rel_diff_part(img, rd, name, bins, v):
    """
    magnitude of relative difference (real vs imaginary)
    """
    plot_rel_diff(
        img,
        "part",
        name,
        isolate_part(rd),
        bins=bins,
        labels=LABELS_PARTS,
        v=v,
    )


def plot_rel_diff_eps(img, rd, name, bins, v):
    """
    magnitude of relative difference (eps orders)
    """
    plot_rel_diff(
        img,
        "eps",
        name,
        isolate_eps(rd),
        bins=bins,
        labels=labels_eps(rd),
        v=v,
        ltit=r"$w$",
    )


def plot_rel_diff_fn(img, rd, name, bins, v):
    """
    magnitude of relative difference (integral functions)
    unused
    """
    plot_rel_diff(
        img,
        "fn",
        name,
        isolate_fn(rd),
        bins=bins,
        labels=labels_fn(rd),
        v=v,
    )


def plot_line(img, sin, sout, serr, name, v):
    """
    integral function G_3 vs t (box)
    unused
    """
    file = img / f"line_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    pin = sin[:, 0]

    # similar to eq 2.13 but it's s23^(2 + eps)*j[box, 1, 1, 1, 1] and one eps order higher
    n = 2
    w = 5 - n
    m = 2 * n
    pout = sout[:, 2, w:, :].reshape(-1, m)
    perr = serr[:, 2, w:, :].reshape(-1, m)

    i = numpy.argsort(pin)
    pin = pin[i]
    pout = pout[i, :]
    perr = perr[i, :]

    u = 3 - n
    labs = numpy.array(
        [
            rf"$\mathrm{{{p}}} \, F_3^{{({i})}}$"
            for i in range(u, 3)
            for p in ("Re", "Im")
        ]
    )

    colours = numpy.repeat(matplotlib.cm.tab10(range(n)), 2, axis=0)
    linestyles = numpy.tile(("solid", "dashed"), n)

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    ax.set_xlabel("$t$")
    ax.set_ylabel("$G_3^{(w)}$")

    ax.set_prop_cycle(linestyle=linestyles, color=colours)

    for out, err, lab in zip(pout.T, perr.T, labs):
        ax.fill_between(pin, out - err, out + err, alpha=0.2)
        ax.plot(pin, out, label=lab)

    ax.legend()

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_line_with_error(img, sin, sout, serr, terr, name, v=True, p=0):
    """
    integral function G_3 vs t (box) with error bars
    unused
    """
    file = img / f"line_errors_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    pin = sin[:, 0]

    pout = sout[:, 2, 4, :].reshape(-1, 2)
    perr = serr[:, 2, 4, :].reshape(-1, 2)
    terr = terr[:, 2, 4, :].reshape(-1, 2)
    # pout = sout[:, 2, 4, 0]
    # perr = serr[:, 2, 4, 0]
    # terr = terr[:, 2, 4, 0]

    i = numpy.argsort(pin)
    pin = pin[i]
    pout = pout[i, :]
    perr = perr[i, :]
    terr = terr[i, :]
    # pout = pout[i]
    # perr = perr[i]
    # terr = terr[i]

    labs = (
        r"$\mathrm{Re} \, F_3^{(2)}$",
        r"$\mathrm{Im} \, F_3^{(2)}$",
    )

    colours = matplotlib.cm.tab10(range(4))

    fig, axs = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
        nrows=2,
        sharex="col",
        gridspec_kw={"hspace": 0},
    )

    axs[1].set_xlabel("$t$")

    axs[0].set_ylabel("$G_3^{(2)}$")

    axs[1].set_ylabel(
        rf"$\mathrm{{Re}} \, F_3^{{(2)}}$ errors{multiplier(p)}", labelpad=25
    )

    for out, err, lab in zip(pout.T, perr.T, labs):
        axs[0].fill_between(pin, out - err, out + err, alpha=0.2)
        axs[0].plot(pin, out, label=lab)

    axs[1].set_prop_cycle(color=colours[2:])
    axs[1].plot(pin, terr[:, 0] * 10**p, label="Testing error")
    axs[1].plot(pin, perr[:, 0] * 10**p, label="Ensemble uncertainty")

    for ax in axs:
        ax.legend()

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_ps_1d(img, x, y, name, labels=None, v=True, p=0):
    """
    Errors binned (taking the mean value per bin) over phase space
    """
    file = img / f"ps_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    x = x[:, 0]  # box input is 1d

    delta = numpy.finfo(numpy.float32).eps
    bins = numpy.histogram_bin_edges(
        x, bins="auto", range=(x.min() - delta, x.max() + delta)
    )

    inds = numpy.digitize(x, bins)

    vals = numpy.array(
        [y[:, inds == i].mean(axis=(1, 2, 3, 4)) for i in range(1, len(bins))]
    )
    vals = numpy.insert(vals, 0, vals[0], axis=0)

    vals *= 10**p

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    ax.set_xlabel(r"$t$")
    ax.set_ylabel(f"Errors{multiplier(p)}")

    ax.step(bins, vals, label=labels)

    ax.set_ylim(bottom=0)

    if labels is not None:
        ax.legend()

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def correlation(
    img,
    y0,
    y1,
    name,
    kind="uncertainty",
    ylabel="Ensemble uncertainty",
    v=True,
    n=100,
    q0=0.002,
    q1=0,
):
    """
    absolute difference (AD) vs ensemble uncertainty (EU) correlation plots
    """

    file = img / f"correlation_{kind}_{name}_{'log' if log else 'lin'}.pdf"
    if v:
        print(f"Plotting {file}")

    x = y0.ravel()
    y = y1.ravel()

    t = max(x.max(), y.max())
    if log:
        delta = numpy.finfo(numpy.float32).eps
        l, u = numpy.log10(
            numpy.quantile(
                numpy.concatenate((x[x > delta], y[y > delta])), (q0, 1 - q1)
            )
        )
        b = numpy.logspace(l, u, n)
    else:
        b = numpy.linspace(0, t, n)

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    xlabel = "Absolute difference"

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    ax.set_xscale("log")
    ax.set_yscale("log")

    _, _, _, im = ax.hist2d(
        x,
        y,
        bins=b,
        norm="log",
    )

    ax.set_aspect("equal", "box")

    fig.colorbar(im, label="Count")

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def t331ZZZM_boundaries(root, n, v):
    """
    scattering region for one-mass double box
    """
    cfg = get_cfg(root, conf_file="ps.json", verbose=v)
    _, s12, cut, __ = ps_params(cfg)

    # s23 < -cut
    s23 = numpy.linspace(-s12 + 2 * cut, -cut, n)
    # s4 > cut
    s4_lower = numpy.full_like(s23, cut)
    # s12 + s23 - s4 > cut
    s4_upper = s12 + s23 - cut

    return s23, ((s4_lower, s4_upper),)


def hcb_boundaries(root, n, v):
    """
    scattering region for heavy crossed box
    """
    cfg = get_cfg(root, conf_file="ps.json", verbose=v)
    scale, m2, cut, __ = ps_params(cfg)

    # 2 * cut < s < scale
    s = numpy.linspace(2 * cut, scale, n)
    # t > -s + cut
    t_lower = -s + cut
    # t < -cut
    t_upper = numpy.full_like(s, -cut)

    return s, ((t_lower, t_upper),)


def tb_boundaries(root, n, v):
    """
    scattering region for top double box
    """
    cfg = get_cfg(root, conf_file="ps.json", verbose=v)
    scale, m2, cut, scut = ps_params(cfg)
    d = 3e-3

    # cut + 4 * m2 < s12 < scale
    s12 = numpy.linspace(cut + 4 * m2 + d, scale, n)

    # s23 < (cut + 2*mt2 + Sqrt[(cut - s12)*(cut + 4*mt2 - s12)] - s12)/2
    s23_1_upper = (
        cut + 2 * m2 + numpy.sqrt((cut - s12) * (cut + 4 * m2 - s12)) - s12
    ) / 2
    # s23 > m2 + scut
    s23_1_lower = numpy.full_like(s12, -m2 + scut)

    # s23 < m2 - scut
    s23_2_upper = numpy.full_like(s12, -m2 - scut)
    # s23 < (cut + 2*mt2 - Sqrt[(cut - s12)*(cut + 4*mt2 - s12)] - s12)/2
    s23_2_lower = (
        cut + 2 * m2 - numpy.sqrt((cut - s12) * (cut + 4 * m2 - s12)) - s12
    ) / 2

    return s12, ((s23_1_lower, s23_1_upper), (s23_2_lower, s23_2_upper))


def region_plot(img, datadir, name, boundaries, xlabel, ylabel, const, pos, v):
    """
    scattering region with boundary points
    """
    file = img / f"region_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    root = datadir / name

    boundary_points_file = root / "boundary_input.csv"
    if v:
        print(f"Reading {boundary_points_file}")
    boundary_points = numpy.loadtxt(boundary_points_file, delimiter=",", ndmin=2)

    x, ys = boundaries(root, 100, v)

    colours = matplotlib.cm.tab10(range(2))

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(R * W, R * W),
    )

    for y0, y1 in ys:
        ax.fill_between(x, y0, y1, color=colours[0])

    ax.scatter(*boundary_points.T, color=colours[1])

    m = 0.1
    if pos == "upper left":
        coords = (m, 1 - m)
    elif pos == "lower left":
        coords = (m, m)
    else:
        raise Exception(f"Position {pos} for plot text unknown")

    ax.text(*coords, const, transform=ax.transAxes)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def zero_mask(cfg, n):
    """
    mask zero outputs
    """
    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    re_z = numpy.array(cfg["zeros"]["re"]).T
    im_z = numpy.array(cfg["zeros"]["im"]).T

    mask = numpy.full((num_fn, num_eps, 2), False)

    mask[*re_z, 0] = True
    mask[*im_z, 1] = True

    return numpy.tile(mask, n)


def errors_by_axes(diff, kind, v):
    """
    print error statistics
    """
    if not v:
        print(f"mean {kind} diff: {diff.mean():.2g}")

    else:
        b = 10
        print(f"{kind} diff means:\n{'all':>{b}}: {diff.mean():.2g}")

        mean_by_part = diff.mean(axis=(0, 1, 2))

        for p, m in zip(("re", "im"), mean_by_part):
            print(f"{p:>{b}}: {m:.2g}")

        mean_by_eps = diff.mean(axis=(0, 1, 3))

        for w, m in enumerate(mean_by_eps):
            print(f"{f'O(eps^{w})':>{b}}: {m:.2g}")

        # print(f"{'worst O':>{b}}: {mean_by_eps.max():.2g}")

        mean_by_fn = diff.mean(axis=(0, 2, 3))

        for i, m in enumerate(mean_by_fn, start=1):
            print(f"{f'f{i}':>{b}}: {m:.2g}")

        # print(f"{'worst f':>{b}}: {mean_by_fn.max():.2g}")


def boundary_positions(img, bpr, ns, args):
    """
    boundary points plotted on phase space
    """
    file = img / "boundary_locations.pdf"
    if args.verbose:
        print(f"Plotting {file}")

    bps = numpy.loadtxt(bpr / "boundary_input.csv", delimiter=",")

    x, ys = t331ZZZM_boundaries(args.data / "t331ZZZM", 100, args.verbose)

    colours = matplotlib.cm.tab10(range(7))

    s = 40
    marker_boundary = "^"
    marker_interior = "s"

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(R * W, R * W),
    )

    for y0, y1 in ys:
        ax.fill_between(x, y0, y1, color=colours[0])

    ax.scatter(
        *bps[18:24].T,
        color=colours[6],
        label=12,
        s=s,
        marker=marker_interior,
    )

    for j in reversed(range(2)):
        ax.scatter(
            *bps[12 + 3 * j : 12 + 3 * (j + 1)].T,
            color=colours[4 + j],
            label=3 * (j + 1),
            s=s,
            marker=marker_interior,
        )

    idx = [i * 4 + 2 + j for i in range(3) for j in range(2)]
    ax.scatter(
        *bps[idx].T,
        color=colours[3],
        label=12,
        s=s,
        marker=marker_boundary,
    )

    for j in reversed(range(2)):
        idx = [i * 4 + j for i in range(3)]
        ax.scatter(
            *bps[idx].T,
            color=colours[1 + j],
            label=3 * (j + 1),
            s=s,
            marker=marker_boundary,
        )

    ax.set_xlabel(r"$s_{23}$")
    ax.set_ylabel(r"$s_4$")

    ax.legend(ncols=2, title=r"Boundary\ \ \ \ \ Interior", reverse=True)

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def boundary_matrix(mat, lab, ticks, p, v):
    """
    matrix figure for boundary point set hyperparameter optimisation results
    unused
    """
    name = lab.lower().replace(" ", "_")
    file = img / f"boundary_matrix_{name}.pdf"
    if v:
        print(f"Plotting {file}")

    mat = mat * 10**p

    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(W, R * W),
    )

    ax.xaxis.set_label_position("top")
    ax.set_xlabel("bulk")

    ax.set_ylabel("boundary")

    pos = range(len(ticks))
    ax.set_xticks(pos, ticks)
    ax.set_yticks(pos, ticks)

    im = ax.matshow(mat, cmap="viridis_r")

    for (i, j), e in numpy.ndenumerate(mat):
        if not math.isnan(e):
            txt = ax.text(
                j,
                i,
                f"{e:.1f}",
                horizontalalignment="center",
                verticalalignment="center",
                color="white",
            )
            txt.set_path_effects(
                [matplotlib.patheffects.withStroke(linewidth=1, foreground="black")]
            )

    ax.set_aspect("equal", "box")

    fig.colorbar(im, label=f"{lab}{multiplier(p)}")

    fig.savefig(file, bbox_inches="tight")

    matplotlib.pyplot.close()


def boundary_optimisation(root, img, args):
    """
    one-mass double box: figures and tables for appendix on optimisation of choice of boundary point set
    unused
    """
    print("# t331ZZZM boundaries")

    t = root / "t331ZZZM" / "boundary-tuning"
    ns = (0, 3, 6, 12)

    boundary_positions(img, t, ns, args)

    vx = numpy.loadtxt(args.data / "t331ZZZM" / "validation_input.csv", delimiter=",")
    vxt = torch.tensor(vx, device=args.device, dtype=torch.float32)

    cfg = get_cfg(args.model / "t331ZZZM", verbose=args.verbose)

    num_fn = cfg["dimensions"]["output"]["functions"]
    num_eps = cfg["dimensions"]["output"]["epsilon_orders"]

    re_z = numpy.ravel_multi_index(numpy.array(cfg["zeros"]["re"]).T, (num_fn, num_eps))

    mask = numpy.full(num_fn * num_eps, False)
    mask[re_z] = True
    mask = numpy.tile(mask, (len(vx), 1))

    vy = numpy.loadtxt(
        args.data / "t331ZZZM" / "re" / "validation_output.csv", delimiter=","
    )
    vy = numpy.ma.array(vy, mask=mask)

    b = args.model / "t331ZZZM" / "boundary-tuning" / "re"

    mat_fl = []
    mat_eadm = []

    if args.verbose:
        print(
            "*key* boundary+bulk: (replica MADs) > mean / diff | ensemble MAD / MMRD | loss"
        )

    for boun in ns:
        row_fl = []
        row_eadm = []

        for bulk in ns:
            if boun == bulk == 0:
                for row in (row_fl, row_eadm):
                    row.append(float("nan"))
                continue

            ads = []
            ys = []
            fls = []
            for repl in range(2):
                name = f"boun{boun:02}bulk{bulk:02}repl{repl:02}"

                tt = t / name

                if not tt.is_dir():
                    raise Exception(
                        f"Replica training stats directory {tt} doesn't exist"
                    )

                stats = {
                    k: v
                    for k, v in zip(
                        ("t", "lr", "avg", "min", "max"),
                        numpy.load(tt / "training-statistics.npy"),
                    )
                }

                fl = stats["avg"][-1]
                fls.append(fl)

                r = b / name

                if not r.is_dir():
                    raise Exception(f"Replica model directory {r} doesn't exist")

                model = load_model(r, args.device, args.verbose)

                with torch.no_grad():
                    y = model(vxt)

                y = y.cpu().numpy()
                ys.append(y)

                ad = abs(vy - y).mean()
                ads.append(ad)

            ey = numpy.array(ys).mean(axis=0)
            ead = abs(vy - ey).mean()

            fls = numpy.array(fls)
            flm = fls.mean()

            if args.verbose:
                ads = numpy.array(ads)
                adm = ads.mean()
                mmrd = abs((vy - ey) / vy).mean()
                print(
                    f"{boun:>2}+{bulk:>2}: ({ads[0]:7.2g} {ads[1]:7.2g}) > {adm:7.2g} / {abs(ads[0]-ads[1]):7.2g} | {ead:7.2g} / {mmrd:7.2g} | ({fls[0]:7.2g} {fls[1]:7.2g}) > {flm:7.2g}"
                )

            row_fl.append(flm)
            row_eadm.append(ead)

        mat_fl.append(row_fl)
        mat_eadm.append(row_eadm)

    mat_fl = numpy.array(mat_fl)
    mat_eadm = numpy.array(mat_eadm)

    # boundary_matrix(mat_fl, "Final epoch loss", ns, 4, args.verbose)
    # boundary_matrix(mat_eadm, "Mean absolute difference", ns, 3, args.verbose)

    matrix = r"""\begin{table}
    \centering
    \begin{tabular}{|c|c c c c|}
        \hline
        \diagbox{boundary}{interior} & 0 & 3 & 6 & 12 \\
        \hline
        """

    for n, row_fl, row_ad in zip(ns, 1e4 * mat_fl, 1e3 * mat_eadm):
        matrix += (
            " & ".join(
                [str(n), *[f"{fl:.1f}/{ad:.1f}" for fl, ad in zip(row_fl, row_ad)]]
            ).replace(" nan/nan", "")
            + " \\\\\n        "
        )

    matrix += r"""\hline
    \end{tabular}
    \caption{
    }
    \label{tab:boundaries}
\end{table}"""

    print(matrix)


def plot_abs_diff_ps_hist(
    img, name, ad, x, xlabel, ylabel, bins=10, norm="linear", v=True, p=2, m=0.05
):
    """
    absolute difference histogrammed over phase space
    """
    filename = img / f"abs_diff_ps_histo_{name}.pdf"
    if v:
        print(f"Plotting {filename}")

    # take part/eps/fn mean
    adf = ad.mean(axis=(1, 2, 3)) * 10**p

    h, xedges, yedges = numpy.histogram2d(x[:, 0], x[:, 1], bins=bins, weights=adf)
    counts, _, _ = numpy.histogram2d(x[:, 0], x[:, 1], bins=bins)
    with numpy.errstate(invalid="ignore", divide="ignore"):
        # take bin mean
        h /= counts

    left = xedges[0]
    right = xedges[-1]
    bottom = yedges[0]
    top = yedges[-1]

    fig, ax = matplotlib.pyplot.subplots(figsize=(W, R * W), constrained_layout=True)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    ax.set_xlim(left - m, right + m)
    ax.set_ylim(bottom - m, top + m)

    im = ax.imshow(h.T, norm=norm, origin="lower", extent=(left, right, bottom, top))

    fig.colorbar(im, label=f"Absolute difference{multiplier(p)}")

    fig.savefig(filename, bbox_inches="tight")

    matplotlib.pyplot.close()


def plot_abs_diff_ps_scatter(
    img,
    name,
    ad,
    x,
    xlabel,
    ylabel,
    norm="linear",
    vmin=None,
    vmax=None,
    s=20,
    p=3,
    v=True,
):
    """
    scatter plot of absolute difference (mean over part/eps/fn) sampled over phase space
    """
    filename = img / f"abs_diff_ps_scatter_{name}.pdf"
    if v:
        print(f"Plotting {filename}")

    adf = ad.mean(axis=(1, 2, 3)) * 10**p

    fig, ax = matplotlib.pyplot.subplots(figsize=(W, R * W), constrained_layout=True)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    pc = ax.scatter(
        x[:, 0],
        x[:, 1],
        s=s,
        c=adf,
        norm=norm,
        vmin=vmin,
        vmax=vmax,
    )

    fig.colorbar(pc, label=f"Absolute difference{multiplier(p)}")

    fig.savefig(filename, bbox_inches="tight")

    matplotlib.pyplot.close()


if __name__ == "__main__":
    args = get_args()

    if args.final:
        matplotlib.pyplot.rc("text", usetex=True)
        matplotlib.pyplot.rc("font", family="serif")

    img = pathlib.Path("img")
    img.mkdir(exist_ok=True)

    root = pathlib.Path("Results")

    print("# training info")

    ps = ("re", "im")

    epoch_time = {
        "box": 4,
        "t331ZZZM": 15,
        "heavycrossbox": 29,
        "topbox": 19,
    }

    epoch_iterations = {
        "box": 1024,
        "t331ZZZM": 512,
        "heavycrossbox": 512,
        "topbox": 512,
    }

    # best 10 by deriv diff over 131072 points
    replicas = {
        "topbox": (
            ["0", "1", "3", "6", "7", "9", "13", "15", "16", "18"],  # re
            ["0", "3", "4", "5", "6", "7", "8", "9", "12", "15"],  # im
        )
    }

    # limited for 4GB RAM
    ddp = (
        {
            "heavycrossbox": 32768,
            "topbox": 65536,
        }
        if str(args.device) == "cuda"
        else {}
    )

    for fam in (
        "box",
        "t331ZZZM",
        "heavycrossbox",
        "topbox",
    ):
        print(f"## {fam}")

        try:
            reps = replicas[fam]
        except KeyError:
            reps = [[str(i) for i in range(10)] for _ in range(2)]

        metrics = get_training_data(root / fam, *reps, v=args.verbose)

        if not args.suppress_statistics:
            fls = [numpy.mean(metrics[:, p, "avg", -1]) for p in ps]
            print(f"final loss: {numpy.mean(fls):.2g}")

            epochs = [numpy.mean([len(x) for x in metrics[:, p, "t"]]) for p in ps]
            mne = numpy.mean(epochs)
            print(f"iterations: {epoch_iterations[fam]*mne:.2g}")

            t = mne * epoch_time[fam] / 60
            print(f"time: {t:.0f} mins = {t / 60:.1f} hrs")

        if not args.suppress_plots:
            plot_training_statistics(
                img, fam, metrics, v=args.verbose, triple=fam != "topbox"
            )

    print("# testing info")

    cachedir = pathlib.Path("cache")
    cachedir.mkdir(exist_ok=True)

    variables = {
        "t331ZZZM": [
            r"$s_{23}$",
            r"$s_4$",
        ],
        "heavycrossbox": [
            r"$s$",
            r"$t$",
        ],
        "topbox": [
            r"$s_{12}$",
            r"$s_{23}$",
        ],
    }

    for names, pr in (
        ("box", 5),
        ("t331ZZZM", 2),
        ("heavycrossbox", 2),
        ("topbox", 2),
    ):
        if isinstance(names, str):
            model_name = data_name = names
        else:
            model_name, data_name = names

        print(f"## {data_name}")
        fam = args.model / model_name

        try:
            reps = replicas[data_name]
        except KeyError:
            reps = [[str(i) for i in range(10)] for _ in range(2)]

        cfg = get_cfg(fam, verbose=args.verbose)

        cachefile = cachedir / f"{model_name}.npz"
        if args.cache and cachefile.is_file():
            if args.verbose:
                print(f"Reading {cachefile}")
            with numpy.load(cachefile) as cachedata:
                input = cachedata["input"]
                output = cachedata["output"]
                sout = cachedata["sout"]
                serr = cachedata["serr"]
        else:
            const = cfg["phase_space"]["const"]

            model = Ensemble(
                fam,
                *reps,
                const=const,
                device=args.device,
                re_z=cfg["zeros"]["re"],
                im_z=cfg["zeros"]["im"],
                v=args.verbose,
            )
            dtype = model.dtype

            input, output = get_testing_data(args.data / data_name, cfg, v=args.verbose)

            xr = torch.tensor(input, device=args.device, dtype=dtype)
            k = torch.full((len(xr), 1), const, device=args.device, dtype=dtype)
            x = torch.cat((xr, k), axis=1)

            nt = len(x)
            print(f"Testing dataset size: {nt}")

            t0 = time.time()
            model.eval(x)
            dur = time.time() - t0
            each = dur / nt
            print(f"Mean evaluation time: {1e6*each:.2g}us")

            sout = model.pair().cpu().numpy()
            serr = model.error_abs().cpu().numpy()

            if args.cache:
                if args.verbose:
                    print(f"Caching to {cachefile}")
                numpy.savez(
                    cachefile,
                    input=input,
                    output=output,
                    sout=sout,
                    serr=serr,
                )

        z = zero_mask(cfg, (len(input), 1, 1, 1))
        output = numpy.ma.array(output, mask=z)
        serr = numpy.ma.array(serr, mask=z)
        sout = numpy.ma.array(sout, mask=z)

        if not args.suppress_statistics:
            print(f"mean std err: {serr.mean():.2g}")

            # dd

            n = 131072

            dd = DerivativeDifference(fam, data_name, args.device, args.verbose)
            models = numpy.array(
                [
                    fam / part / replica
                    for part, part_reps in zip(("re", "im"), reps)
                    for replica in part_reps
                ]
            )

            partition_size = ddp[data_name] if data_name in ddp else n
            num_partitions = n // partition_size
            diffs = dd(models, partition_size, num_partitions).detach().cpu()

            diffs = numpy.array(numpy.split(diffs, 2, axis=0))
            n_reps = len(reps[0])
            n_in = cfg["dimensions"]["input"]["variables"]
            zz = zero_mask(cfg, (1,))
            zz = numpy.expand_dims(zz, (0, 1, 2))
            zz = numpy.tile(zz, (num_partitions, partition_size, n_reps, n_in, 1, 1, 1))
            diffs = numpy.moveaxis(diffs, (2, 4, 1, 5, 6, 3, 0), (0, 1, 2, 3, 4, 5, 6))
            diffs = numpy.ma.array(diffs, mask=zz)

            print(f"deriv diff: {diffs.mean():.2g}")

        # ad = |h-g|

        ad = abs(output - sout)

        # rd = |h-g|/h

        rd = ad / abs(output)

        if not numpy.isfinite(rd).all():
            print("Warning: rd not finite")

        # r = log g/h

        with numpy.errstate(invalid="ignore", divide="ignore"):
            # signs will match (so bare ratio positive) except where abs err > val magnitude
            r = numpy.log(abs(sout / output))

        if not args.suppress_statistics:
            errors_by_axes(ad, "abs", args.verbose)
            errors_by_axes(rd, "rel", args.verbose)
            print(f"log(ratio).mean: {r[numpy.isfinite(r)].mean():.2g}")

        if not args.suppress_plots:
            binsa = 50
            plot_rel_diff_eps(img, rd, data_name, binsa, v=args.verbose)

            binsr = 70
            plot_ratio_eps(img, r, data_name, binsr, v=args.verbose, p=pr)

            # correlation(
            #     img,
            #     ad,
            #     serr,
            #     data_name,
            #     p=pr,
            #     v=args.verbose,
            #     log=True,
            # )

            # # WIP requires autodiffable ensemble
            # xr = xr.T
            # xr.requires_grad_()
            # des = dd.get_des(xr)
            # ddt = dd.get_diff(xr, des, f)
            # correlation(
            #     img,
            #     ad,
            #     ddt,
            #     data_name,
            #     kind="derivative",
            #     ylabel="ADD",
            #     p=pr,
            #     v=args.verbose,
            #     log=True,
            # )

            plot_abs_diff_part(img, ad, data_name, binsa, v=args.verbose)
            plot_abs_diff_eps(img, ad, data_name, binsa, v=args.verbose)

            if data_name == "box":
                plot_ps_1d(
                    img,
                    input,
                    numpy.array([ad, serr]),
                    data_name,
                    labels=(
                        "Testing absolute difference",
                        "Ensemble uncertainty",
                    ),
                    p=5,
                    v=args.verbose,
                )
            elif data_name == "t331ZZZM":
                plot_abs_diff_ps_hist(
                    img,
                    data_name,
                    ad,
                    input,
                    *variables[data_name],
                    bins=100,
                    p=2,
                    v=args.verbose,
                    norm="log",
                )
                # plot_abs_diff_ps_scatter(
                #     img,
                #     data_name,
                #     ad,
                #     input,
                #     *variables[data_name],
                #     s=1,
                #     p=2,
                #     v=args.verbose,
                # )
            elif data_name == "heavycrossbox":
                plot_abs_diff_ps_scatter(
                    img,
                    data_name,
                    ad,
                    input,
                    *variables[data_name],
                    s=30,
                    p=3,
                    v=args.verbose,
                    norm="log",
                )
            elif data_name == "topbox":
                plot_abs_diff_ps_scatter(
                    img,
                    data_name,
                    ad,
                    input,
                    *variables[data_name],
                    s=30,
                    p=3,
                    v=args.verbose,
                    norm="log",
                    vmin=None,
                    vmax=None,
                )

    if not args.suppress_plots:
        print("# region plots")
        region_plot(
            img,
            args.data,
            "t331ZZZM",
            t331ZZZM_boundaries,
            *variables["t331ZZZM"],
            r"$s_{12}=2.5$",
            "upper left",
            v=args.verbose,
        )
        region_plot(
            img,
            args.data,
            "heavycrossbox",
            hcb_boundaries,
            *variables["heavycrossbox"],
            r"$m^2=1$",
            "lower left",
            v=args.verbose,
        )
        region_plot(
            img,
            args.data,
            "topbox",
            tb_boundaries,
            *variables["topbox"],
            r"$m_\mathrm{t}^2=1$",
            "lower left",
            v=args.verbose,
        )
