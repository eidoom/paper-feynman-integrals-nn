\documentclass[11pt, a4paper]{article}

\usepackage[numbers,compress]{natbib}
\usepackage[hidelinks]{hyperref}
\usepackage[left=2cm,right=2cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage{tikz}
\usepackage{comment}
\usepackage{amsmath}

\newcommand{\RM}[1]{\textcolor{cyan}{{\bf\tt [RM: #1]}}}
\newcommand{\SZ}[1]{\textcolor{red}{{\bf\tt [SZ: #1]}}}
\newcommand{\FC}[1]{\textcolor{blue}{{\bf\tt [FC: #1]}}}

\renewcommand{\i}{\mathrm{i}}
\newcommand{\eps}{\epsilon}

\title{Reply to report on JHEP\_176P\_1223}
\author{Francesco Calisto, Ryan Moodie, Simone Zoia}

\begin{document}

\maketitle

We are very grateful to the referee for the thorough and useful feedback. 
We hereby submit a revised version of the paper in which all points of criticism are addressed.
A comprehensive list of changes and our replies to the referee's comments are given below, in the same order as in the report.
The cited references correspond to the bibliography of the article.


\begin{enumerate}
\item \textit{However, I share some of the previous referee’s concerns that this study
is quite in its infancy, and it is not at this point clear whether the proposed
method is going to outperform other techniques in complex cases. I believe
that this should not bar publication, but the authors should perhaps stress
more, e.g.\ in the introduction and abstract, that this is mostly exploratory
work and more study is necessary (like they do in the conclusions).}

We have modified the abstract to emphasise that this is an exploratory study by replacing ``We present a new approach for [...]'' with ``We perform an exploratory study of a new approach for [...]'' in the first sentence. 
Furthermore, we have added the following clarification in the introduction:
\begin{quote}
These exploratory applications prove the feasibility of our approach, and allow us to identify the aspects which require further study in order to make this a fully developed alternative to current numerical methods.
\end{quote}


\item \textit{In particular, the authors study analytically-complex integrals, which
however have a simple $2 \to 2$ kinematics structure, with a not-too complicated
threshold structure. It is difficult for me to extrapolate their result to more
complex kinematic situations. Do the authors have any experience on this?
Can they comment?}

At first we tried some five-point Feynman integrals, with significantly more complicated $2 \to 3$ kinematics. 
Their complexity however made them unsuitable for an exploratory study.
The more complicated kinematics and the larger number of master integrals in fact imply that larger neural networks have to be employed. 
This increases the training time, making it substantially more time-consuming to tune the hyper-parameters.
Since this was the first study of this kind and we had to start from zero, we preferred to prioritise cases which are more complicated from the analytic point of view, but which require smaller networks. 
This allowed us to perform many tests, and therefore to learn many lessons which will help us to tackle cases with more complicated kinematics in the future.



\item \textit{Another critical point to address is the reliability of the output. The
authors show that the NN output agrees well with the exact result, but the
only place where I see a comparison between their estimated uncertainty and
the actual error of their output is Fig.~3f. I believe they should expand a bit
on this.}

Table~3 gives a summary of ensemble uncertainty and testing errors for all the examples considered.
In order to address point~10 below, we have added figures in a new appendix which provide further visual information about the reliability of our models.
We however emphasise that, as of now, we cannot provide a reliable estimate of the uncertainty. 
We have considered two proxies for that: the ensemble uncertainty and the differential error.
The ensemble uncertainty is a fairly well established concept, but only captures the part of the uncertainty due to the random initialisation of the neural networks. 
It is therefore not expected to be fully compatible with the testing error.
The differential error (defined in eq.~(3.17)) instead informs us about how well our model solves the differential equations. 
In the last paragraph of section~4 we comment on the fact that the differential error appears to be correlated to the testing uncertainty. 
We expect that understanding quantitatively this correlation would allow us to provide a reliable estimate of the model uncertainty.
We have however left this for further study.
We have clarified this in the conclusions by specifying that by ``how well the model satisfies the DEs'' we mean the differential error, and by adding the following sentence:
\begin{quote} 
Our preliminary investigation indicates that these proxies may be used to provide a reliable estimate of the uncertainty, although further study is required in this direction, in particular to characterise the correlation between the differential error and the testing error. 
\end{quote}


\item \textit{In the introduction, pag. 2, the authors state that their method is fast,
but this comes ``at the cost of a lower control over the precision''. As
I’ve mentioned, this is critical for Feynman integral calculations so I
would like the authors to comment on how they plan to address this
shortcoming}

The phrase ``at the cost of a lower control over the precision'' without further explanation in the introduction is indeed confusing. 
We have replaced it with the following:
\begin{quote}
However, inherent precision limits mean this method cannot be competitive with analytical results, whenever they are available.~\footnote{For example, using 32-bit floating-point numbers implies a hard precision limit of about 8 digits.}
\end{quote}
As it is clear from our results, we are not yet limited by the floating-point arithmetic.
Some details about how one could improve on this are given in the paragraph of the conclusion starting with ``The question of how to improve the fitting performance''.
We have expanded this paragraph by adding the following sentences:
\begin{quote}
A hard limit on the achievable performance is set by the floating-point arithmetic. 
We have verified that we have not yet reached this limit by observing that training the models with double-precision arithmetic does not improve their performance.
One way to improve consists in performing further hyper-parameter tuning. 
For example, increasing the width and depth of the neural networks may enhance their expressivity, while changes in the optimisation procedure may allow us to find lower minima of the loss function. 
We stress that, in this study, we have performed limited hyper-parameter tuning, stopping when we deemed that the achieved performance was sufficient to prove the feasibility of our method.
\end{quote}


\item \textit{A pedantic comment: technically the authors’ definition of the Feynman
integral eq.~(2.3) includes a $\mu^{2 \epsilon}$ factor, so eq.~(2.13) should not read
$(-s)^{-\epsilon}$ but rather $(-s/\mu^2)^{-\epsilon}$. Also, this section contains well-known
material so I expect it to be here only for people who have no experience of Feynman integrals. 
In this spirit, explicitly spelling out $(-s)^{-\epsilon} \to s^{-\epsilon} e^{\i \pi \eps}$ may be beneficial (though not critical)}

We set $\mu = 1$, as mentioned below eq.~(2.4). 
We agree that the analytic continuation deserves further discussion. 
It is however not sufficient to replace $(-s)^{-\epsilon} \to s^{-\epsilon} e^{\i \pi \eps}$, as eq.~(2.13) contains more complicated multivalued functions. 
We have added the following footnote after eq.~(2.13) to clarify how the analytic continuation may be performed.
\begin{quote}
The expression in eq.~(2.13) is well defined in the Euclidean region ($s<0 \land t<0$).
The analytic continuation to the physical region ($s>0 \land t<0$) is obtained by adding a small positive imaginary part to both $s$ and $t$. 
For example, this amounts to replacing $\log(t/s)$ with $\log(-t/s)+\i \pi$.
\end{quote}


\item \textit{Footnote 3: perhaps add that this is in general not possible beyond one loop}

We have implemented the suggested change.


\item \textit{After eq.~(2.20), the authors claim that the choice of which variable one
wants to exclude has an impact on the NN fit. They briefly return
to this point later, but I would add here some comment about the
reason behind it. Also, at pag.~11 they claim that this choice must be
fine-tuned. How did they do it in practice? What is the underlying
principle one should follow?}

We have added the following sentences after eq.~(2.20) to clarify this point:
\begin{quote}
These choices in fact affect the range of scales in the inputs and the target functions within the chosen kinematic region.
Stochastic gradient descent algorithms are in fact highly sensitive to the range of scales involved in the problem, and train optimally when all scales are of the same order.
These choices should therefore be done so as to minimise ---~as much as possible~--- the range of scales in the problem.
This is part of the hyper-parameter tuning, which in this study we performed by hand.
\end{quote}


\item \textit{In the middle of the paragraph after eq.~(3.2), they authors introduce
the concept of soft constraint but never either properly define it or
actually use it in any substantial way. I would either rephrase that
sentence or add a reference}

The notion of ``soft constraint'' is indeed unnecessary and potentially confusing. 
We have removed~it.


\item \textit{For readability: after eq.~(3.7) the authors use $n_F$, which was however
only mentioned en-passant at the very beginning of the manuscript. I
would add a reminder that $n_F$ is the number of master~integrals}

We have implemented the suggested change.


\item \textit{At pag.~11, the authors claim they uniformly sample their
energy-angle parametrisation. In view of potential extensions to more complex
kinematics, it would be interesting if the authors could comment on how
this choice affects the NN fit. They have some sentence on phase space
coverage later on, but some comment here would be beneficial. What
would happen if a different sampling method would be adopted? For
example, could a better sampling improve the small-t error estimate
shown in Fig.~3f?}

We commented on this aspect by adding the following sentences to section~3.2:
\begin{quote}
While we did not experiment with different phase-space distributions, we expect this to have an impact on the model performance.
A given phase-space generator may in fact sample more densely where the target functions vary the most, and this should be reflected in the training.
For this reason, it is important to use the same phase-space generator in the training as in the intended application, e.g.\ the Monte Carlo integration to compute a cross section.
Firstly, this will provide the optimal training input distribution.
Secondly, the uncertainty estimates discussed below are tied to the input distribution used during the training, and therefore only make sense in an application if it employs the same distribution.
\end{quote}


\item \textit{In the ``training'' section, I believe that the authors should explain in a
few words the concept of ``dynamic random sample of the inputs''}

We have added the following sentence to make this concept clearer:
\begin{quote}
In other words, instead of having a fixed input sample, we have a different one for each iteration, which is computed on the fly by sampling the input variables according to some chosen phase-space distribution.
\end{quote}


\item \textit{At pag.~14 the authors claim that they use 10 replicas, but that this is
ad hoc. I find this a good-enough justification for an exploratory study,
but the authors should stress that more work is needed. Or, if they have
any idea on how to choose the number of replicas, they should comment
on it. In my mind, 10 seems quite a small set: without moving from
HEP, typical sets for PDFs fits include 100 to 1000 replicas, to describe
functions which are probably smoother than the ones considered here}

We agree that more work is required to establish the appropriate number of replicas.
However, we believe that 10 replicas is not a small set in this context.
This choice is inspired by the work done in refs.~[102,103] (as well as several subsequent works by both groups), where the authors use ensembles of 20 replicas, and ref.~[55], where 8 replicas are used.
In some tests we did not observe an advantage in using 20 rather than 10 replicas, and hence decided to proceed with 10 for this exploratory study.
We have added the appropriate references to clarify that the number of replicas is drawn from previous works, and stressed that further study is required:
\begin{quote}
Inspired by refs.~[55, 102, 103], we use ten replicas in the ensemble, finding it to provide a suitable distribution, albeit in an ad hoc manner. More work will be necessary to establish the appropriate number of replicas.
\end{quote}
An important difference with respect to the case of PDFs is that we are targeting very ``clean'' data, which are essentially exact at the working precision, as opposed to the real-world experimental data used for PDF fits. 
We believe this distinction may justify the substantially smaller number of replicas used in our and the cited works, although we reiterate that further study is necessary in order to draw definitive conclusions in this regard.


\item \textit{I think the ``result'' section has a major flaw that should be corrected:
namely, the authors do not show the same output for the all the different examples. 
A plot like the one in Fig.~3a-f must be replicated for
all the examples (without removing the scattering region plot, which
is useful). Most importantly, I believe that a curve like Fig.~3f must
be shown for all their example. This is the only curve that shows
that their error estimate is reliable, and as such is paramount for given
credibility to their method. As the authors themselves claim, having
relatively good accuracy is not enough for Feynman integrals as very
large cancellations are typical. Having a good error estimate is then
crucial, and the authors must show they have one (even if it’s just in
its infancy and more work is needed)}

We showed subsets of the plots in Fig.~3 for the other examples merely for the sake of conciseness, but
we agree with the referee that this was not the best choice.
We have now added a new appendix (B) collecting the same plots for all examples, together with a number of sentences throughout the main text to refer to it.
However, we recall that the ensemble uncertainty plotted in Fig.~3f is not the error estimate of our method.
In general, the ensemble uncertainty is expected to catch only the part of the uncertainty due to the random initialisation of the network.
The fact that in Fig.~3f the ensemble uncertainty is comparable to the (absolute) testing error is a hint that, in this case, the other sources of uncertainty are small.
This is not expected in general, and we refer to our reply in point~3 regarding the uncertainty estimate of our method.
We however realised that the original sentence describing Fig.~3f and the comparison between ensemble uncertainty and testing error may mislead the readers into believing that the two are expected to match. 
We have modified it as follows to clarify:
\begin{quote}
In this case, the ensemble uncertainty appears to be a good estimate of the uncertainty of the fit, as it is compatible with the testing error throughout the phase-space region under consideration.
We recall however that the ensemble uncertainty is expected to catch only the part of the uncertainty due to the random initialisation of the NNs.
We discuss the problem of estimating the uncertainty within our method in section~4.5.
\end{quote}

\item \textit{A similar comment: for the most complex case the authors only show
their ``best'' fitting curve. I think it would be much better to show all
the three results, so that one can compare with the simpler examples
shown earlier}

Other than noise, the other curves are in this case qualitatively the same. 
We do not show them simply to keep the figure cleaner and more readable. 
While the latter motivation is already stated in the discussion of fig.~9b in sec.~4.4, we realised that the former observation was not.
We have now added the following sentence to clarify:
\begin{quote}
The other curves are in this case qualitatively the same.
\end{quote}


\item \textit{For their most complex example, the learning curve does not seem to
have converged at all (Fig.~9b). More in general, it is not clear to me
what is the criterion the authors adopt for stopping the traning (and
avoid overlearning). This can be quite delicate, so I would like the
authors to comment on it}

While we agree with the referee that the learning curve has not visually reached a plateau, 
we used the same criterion to stop the training as for the other examples.
We added the following sentence to address this:
\begin{quote}
The learning curve ends in a region which is not visually a plateau due to a setting that terminates training a fixed number of epochs after the learning rate has reached the minimum value of $10^{-8}$ (see section~3.4).
\end{quote}

Furthermore, we expanded the description of the scheduler (\texttt{ReduceLROnPlateau}) in section~3.4 by adding the following footnote:
\begin{quote}
This scheduler keeps track of the minimum epoch loss, which we call \textit{best}.
If, after the \textit{patience} number of epochs, the epoch loss does not drop below $\textit{best}\times(1+\textit{threshold})$, the learning rate is reduced by multiplying by the \textit{factor}.
After the learning rate is reduced, the scheduler waits the \textit{cooldown} number of epochs before starting again.
\end{quote}



\item \textit{Again in the most complex example, the authors claim they discarded
``bad'' replicas and re-trained. This is extremely dangerous, and it is
well-known that such a procedure can lead to extremely biased results
that seem to look fine. It would be much better to train enough replicas
such that these outliers do not carry any statistical weight. I understand that this may be beyond an exploratory study, but I would like
the authors to comment on this point and either state that this is not
acceptable in the long run, or provide argument to justify it. They
should also be more detailed on the criterion that they use to decide
when a replica is ``bad''}

We thank the referee for bringing up this important aspect, which was not sufficiently highlighted in the previous version of our article.
We have expanded the relevant paragraph as follows: 
\begin{quote}
For this example, we found $\approx 50\%$ ($\approx 40\%$) of the replica trainings for the real (imaginary) part to fail, having a differential error in validation up to a few times larger than the others.
Given the exploratory nature of this study, we decided to train a larger number of replicas, and keep in the final ensemble the ten which perform the best according to the differential error.
Further study is required to understand the onset of this phenomenon, and mitigate it.
    For example, improvements in the optimisation algorithm have been shown to reduce the fraction of discarded replicas from $30\%$ to $1\%$ in Parton-Distribution-Function fits~[122].
    Furthermore, more sophisticated algorithms for selecting replicas to boost the performance of the ensemble are available in the machine-learning literature (see e.g.\ ref.~[123]), and it would be interesting to test them in our approach as well.
\end{quote}



\item \textit{It is interesting that in the most complex case the ``simplest'' part of the
result (i.e.\ the lowest order in the $\eps$ expansion) is the one where the NN
performs worst (e.g.\ Fig.~9c). Do the authors have an understanding
of this?}

A potential explanation is the following.
The neural network learns by minimising the loss function.
What matters, in this context, is not the analytic complexity of a given target function, but rather the relative size of the term in the loss function associated with it.
If the terms in the loss function constraining a target function are much larger than the others, they will be minimised more effectively, resulting in a better performance for that target function.
For this reason one may consider multiplying the terms in the loss function by parameters to tune the relative magnitude.
An example of this is discussed in footnote 8 for the relative weight of the DE and boundary terms in the loss function.
By tuning such extra parameters, one should be able to change arbitrarily the relative performance of the various target functions.
Adding further parameters would however make the model and the hyper-parameter tuning more complicated, and we deemed this was beyond the scope of our preliminary exploration.
We stress however that, while we believe this explanation to be reasonable, it is as of now a speculation, and we therefore decided to omit it from the paper.


\item \textit{Similarly, their relative error plots often show structured patterns (e.g.\
the $w=0$ curve if Fig.~7c, or the $w = 4$ one in Fig.~c). What is
driving this behaviour?}

We believe that our answer to point~17 addresses this comment as well.


\item \textit{In the next-to-last paragraph of pag.~25, the authors interestingly point
out that the NN performs well even close to singularities. Can they be
a bit more quantitative? E.g.: how far in the ``extrapolation'' region
(=how close to the singularity) can they go without losing accuracy?}

In that paragraph we are referring to the spurious singularities, which are singularities of the matrices in the DEs but not of the solutions.
The spurious singularities are excluded with cuts only from the training dataset, and the models extrapolate to the entire region surrounding ---~and including~--- them.
The cuts on the physical singularities are instead hard cuts. 
How close the evaluation should go to the physical singularities depends on the intended application and should be set from the start.
We have added the following sentences at the end of that paragraph to highlight this distinction: 
\begin{quote}
We recall that we instead treat the cuts on the physical singularities as hard cuts.
In other words, we did not verify whether the models extrapolate beyond them.
While it would be interesting to study this aspect as well, setting the physical cuts from the start so that they match the cuts in the intended application would ensure an optimal performance.
\end{quote}


\item \textit{Apart from the above points, I also have the following curiosity: the authors
designed a NN that learns all the $\eps$ orders at once. I understand this is easy
to implement, but have they thought of somehow separating the different $\eps$
orders?}

This is a very interesting idea, which we also considered at first.
There are two reasons why we decided to design a NN that learns all the $\eps$ orders at once.
The first is the simplicity of the implementation, as the referee has pointed out. 
The second is that learning the various orders in $\eps$ separately would introduce a degradation of the performance of the models for increasing order in $\eps$.
When learning all orders in $\eps$ at once, the inputs for the training are the boundary values and the DEs.
The latter are known analytically, and the former can be considered as exact given their high precision.
If we instead separated the orders in $\eps$, the MI coefficients at a given order would be coupled through the DEs to themselves and ---~crucially~--- to the lower-order coefficients, for which we must have already a model trained in a previous stage.
This follows from the fact that the DEs do not have a canonical form, i.e., the dependence on $\eps$ in the connection matrices is not factorised but polynomial.
As a result, the training of the model for the coefficients at a given $\eps$-order would take as input ---~in addition to the DEs and to the exact boundary values~--- also the models of the lower orders.
As the latter are constrained in precision by the inherent limits of the method, this introduces a degradation with increasing $\eps$-order which will make it difficult to achieve a sufficient accuracy in the higher orders.

A similar idea which we consider promising is to use this method to learn only the higher orders in $\eps$, assuming that the lower orders are known analytically.
There are a several cases in the literature where, for example, two-loop integrals contain elliptic (or more complicated) functions only at order $\eps^0$.
This is not uncommon, because the poles of the two-loop amplitudes are related to higher $\eps$-orders of one-loop amplitudes, which are polylogarithmic.
In this situation, one could express the first three $\eps$-orders (from $1/\eps^4$ to $1/\eps$) of the two-loop integrals in terms of well understood functions, and then write down a system of DEs only for the remaining, most complicated, $\eps^0$ order.
A neural network could then be constructed to learn the latter from the knowledge of the lower orders, the DEs, and boundary values.


\end{enumerate}


We thank one more time the referee for their valuable comments.
We believe that the changes we made to address their feedback have significantly improved our paper in terms of clarity of scope and of explanation.
We hope that this new version will be suitable for publication in JHEP.\\

Best regards,\\

\noindent
Francesco, Ryan and Simone


\end{document}
