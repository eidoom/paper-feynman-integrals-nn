import numpy
data = numpy.load("/Users/francescocalisto/Desktop/Results/topbox/re/30/training-statistics.npz")

epoch_end_times = data["epoch_end_times"]
learning_rate_values = data["learning_rate_values"]
training_loss_values = data["training_loss_values"]

mmean = training_loss_values.mean(axis=1)
mmin = training_loss_values.min(axis=1)
mmax = training_loss_values.max(axis=1)

output = numpy.stack(
    (
        epoch_end_times,
        learning_rate_values,
        mmean,
        mmin,
        mmax,
    )
)

numpy.save("/Users/francescocalisto/Desktop/FI-NN/paper-feynman-integrals-nn/Results/topbox/10-replicas/re/17/training-statistics.npy", output)

