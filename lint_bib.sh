#!/usr/bin/env bash

bibfile=bibliography.bib

bibtool -i $bibfile -r bibtool.rsc -o $bibfile
