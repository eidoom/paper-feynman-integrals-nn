#!/usr/bin/env python3

import argparse, pathlib
import numpy

FILENAME = "training-statistics"


def slim(indir, outdir):
    data = numpy.load(indir / f"{FILENAME}.npz")

    epoch_end_times = data["epoch_end_times"]
    learning_rate_values = data["learning_rate_values"]
    training_loss_values = data["training_loss_values"]

    mmean = training_loss_values.mean(axis=1)
    mmin = training_loss_values.min(axis=1)
    mmax = training_loss_values.max(axis=1)

    output = numpy.stack(
        (
            epoch_end_times,
            learning_rate_values,
            mmean,
            mmin,
            mmax,
        )
    )

    outdir = (
        pathlib.Path("Results", indir.parts[-3], "10-replicas", *indir.parts[-2:])
        if outdir is None
        else outdir
    )
    outdir.mkdir(parents=True, exist_ok=True)

    numpy.save(outdir / f"{FILENAME}.npy", output)


def get_args():
    parser = argparse.ArgumentParser(
        description="""
Translate full training statistics files to slim versions.
"""
    )
    parser.add_argument(
        "mode", choices=("s", "l"), help="Mode: [s]ingle file or [l]oop directory"
    )
    parser.add_argument(
        "input",
        type=pathlib.Path,
        help="Input directory for: [s] replica; [l] integral family.",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=pathlib.Path,
        metavar="O",
        help="Set custom output directory.",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()

    if args.mode == "s":
        slim(args.input, args.output)

    elif args.mode == "l":
        for part in ("re", "im"):
            for rep in (args.input / part).iterdir():
                if rep.is_dir():
                    slim(rep, args.output)
